package com.example.consumingrest;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RetornoPesquisaDto {

	private Integer fce;
	private String razaoSocial;
	private String cnpj;
	private LocalDateTime dtEntrada;
	private LocalDate dtValidade;
	private String fase;
	private String faseDescricao;
	private String numeroDocumento;
	private String municipio;
	private String codigo;
	private String tipoLicencaDescricao;
	private String tipoLicencaResumo;
	private String atividadeConsema;
	private boolean municipalizado = false;

	public Integer getFce() {
		return fce;
	}

	public void setFce(Integer fce) {
		this.fce = fce;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public LocalDateTime getDtEntrada() {
		return dtEntrada;
	}

	public void setDtEntrada(LocalDateTime dtEntrada) {
		this.dtEntrada = dtEntrada;
	}

	public String getFase() {
		return fase;
	}

	public void setFase(String fase) {
		this.fase = fase;
	}

	public String getFaseDescricao() {
		return faseDescricao;
	}

	public void setFaseDescricao(String faseDescricao) {
		this.faseDescricao = faseDescricao;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Override
	public String toString() {
		return "RetornoPesquisaDto [fce=" + fce + ", razaoSocial=" + razaoSocial + ", cnpj=" + cnpj + ", dtEntrada="
				+ dtEntrada + ", fase=" + fase + ", faseDescricao=" + faseDescricao + ", numeroDocumento="
				+ numeroDocumento + ", municipio=" + municipio + ", codigo=" + codigo + "]";
	}

	public String getTipoLicencaDescricao() {
		return tipoLicencaDescricao;
	}

	public void setTipoLicencaDescricao(String tipoLicenca) {
		this.tipoLicencaDescricao = tipoLicenca;
	}

	public String getAtividadeConsema() {
		return atividadeConsema;
	}

	public void setAtividadeConsema(String atividadeConsema) {
		this.atividadeConsema = atividadeConsema;
	}

	public String getTipoLicencaResumo() {
		return tipoLicencaResumo;
	}

	public void setTipoLicencaResumo(String tipoLicencaResumo) {
		this.tipoLicencaResumo = tipoLicencaResumo;
	}

	public LocalDate getDtValidade() {
		return dtValidade;
	}

	public void setDtValidade(LocalDate dtValidade) {
		this.dtValidade = dtValidade;
	}

	public boolean isMunicipalizado() {
		return municipalizado;
	}

	public void setMunicipalizado(boolean municipalizado) {
		this.municipalizado = municipalizado;
	}
}