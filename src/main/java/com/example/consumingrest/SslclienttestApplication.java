package com.example.consumingrest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class SslclienttestApplication {

	private static final Logger log = LoggerFactory.getLogger(SslclienttestApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SslclienttestApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	@Bean
	public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> request = new HttpEntity<String>("{\"fce\":\"543774\"}", headers);

		return args -> {
			ResponseEntity<List<RetornoPesquisaDto>> rateResponse = restTemplate.exchange(
					"https://api.ima.sc.gov.br/consulta", HttpMethod.POST, request,
					new ParameterizedTypeReference<List<RetornoPesquisaDto>>() {
					});
			List<RetornoPesquisaDto> ret = rateResponse.getBody();
			log.info(ret.toString());
		};

	}

}